package com.hackerrank.stocktrade.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackerrank.stocktrade.model.User;
import com.hackerrank.stocktrade.repository.UserRepository;
import com.hackerrank.stocktrade.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public void createUser(User user) {
		Optional<User> userFound = userRepository.findById(user.getId());
		if (!userFound.isPresent()) {
			userRepository.save(user);
		}
	}

}
