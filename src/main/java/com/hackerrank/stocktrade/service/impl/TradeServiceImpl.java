package com.hackerrank.stocktrade.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackerrank.stocktrade.exception.BadResourceRequestException;
import com.hackerrank.stocktrade.exception.NotFoundException;
import com.hackerrank.stocktrade.model.Trade;
import com.hackerrank.stocktrade.repository.TradeRepository;
import com.hackerrank.stocktrade.service.TradeService;
import com.hackerrank.stocktrade.service.UserService;

@Service
public class TradeServiceImpl implements TradeService {

	@Autowired
	private TradeRepository tradeRepository;

	@Autowired
	private UserService userService;

	@Override
	public void createTrade(Trade trade) {
		userService.createUser(trade.getUser());
		Optional<Trade> existingTrade = tradeRepository.findById(trade.getId());
		if (existingTrade.isPresent()) {
			throw new BadResourceRequestException("Trade already exists");
		}
		tradeRepository.save(trade);
	}

	@Override
	public void deleteAllTrades() {
		tradeRepository.deleteAll();
	}

	@Override
	public Trade getTradeById(Long id) {
		Optional<Trade> trade = tradeRepository.findById(id);
		if (trade.isPresent()) {
			return trade.get();
		}
		throw new NotFoundException("Trade not found.");
	}

	@Override
	public List<Trade> getAllTrades() {
		return (List<Trade>) tradeRepository.findAll();
	}

	@Override
	public List<Trade> findByShares(Integer shares) {
		return tradeRepository.findByShares(shares);
	}

}
