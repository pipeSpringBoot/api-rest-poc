package com.hackerrank.stocktrade.service;

import com.hackerrank.stocktrade.model.User;

public interface UserService {

	void createUser(User user);

}
