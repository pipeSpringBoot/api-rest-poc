package com.hackerrank.stocktrade.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hackerrank.stocktrade.model.Trade;
import com.hackerrank.stocktrade.service.TradeService;

@RestController
@RequestMapping(value = "/trades")
public class TradesController {

	@Autowired
	private TradeService tradeService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void addNewTrade(@RequestBody @Valid Trade trade) {
		tradeService.createTrade(trade);
	}

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public List<Trade> returnAllTrades() {
		return tradeService.getAllTrades();
	}

	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Trade returnTradeById(@PathVariable Long id) {
		return tradeService.getTradeById(id);
	}

	/**
	 * Just for try
	 * 
	 * @param shares
	 * @return
	 */
	@GetMapping("/shares/{shares}")
	@ResponseStatus(HttpStatus.OK)
	public List<Trade> findByShares(@PathVariable Integer shares) {
		return tradeService.findByShares(shares);
	}
}
