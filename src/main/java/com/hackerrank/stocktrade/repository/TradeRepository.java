package com.hackerrank.stocktrade.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hackerrank.stocktrade.model.Trade;

@Repository
public interface TradeRepository extends CrudRepository<Trade, Long> {

	List<Trade> findByShares(Integer shares);
}
