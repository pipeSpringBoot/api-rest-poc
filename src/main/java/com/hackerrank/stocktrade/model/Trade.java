package com.hackerrank.stocktrade.model;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Trade {

	@Id
	private Long id;
	private String type;
	@OneToOne(targetEntity = User.class)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;
	private String symbol;
	private Integer shares;
	private Float price;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Timestamp timestamp;

}